'use strict';

App.controller('ItemController', ['$scope', 'ItemService', function($scope, ItemService) {
	
			var self = this;
			self.item={id:null, name:'', sellingPrice:null, minimumNumberOfItems:null, numberOfitemsInContainer:null, storedLocation:''
				, countWord:'', category:null, modail:null, container:null, itemReference:null};
			self.items=[];
			self.displayedItems=[];
			self.category={id:null, name:'', parentCategory:null, containerType:null, isLeaf:true, binaryCode:''};
			self.selectedParentCategory={};
			self.allItemReference=[];
			self.currentContainerParentCategories=[];
			self.currentContainerCategories=[];
			self.filteredCategories=[];
			self.filteredSelectedCategoryModails=[];
			self.currentContainerModails=[];
			self.containers=[];
	        self.containersType=[];
	        self.openCreateOrListItems = false;
	        self.createOrListItemsTitle = 'Create New Item';
			self.pageSize = 10;

	        
	        
	        self.submitItem = function() {
        			self.createOrUpdateItem();
	        };
            
	        
	        self.createOrUpdateItem = function(){
	        	ItemService.createOrUpdateItem(self.item).then(
	        			function(response){
	        				self.reset();
	        				self.openCreateOrListItems = !self.openCreateOrListItems;
	        				self.createOrListItemsTitle = 'Create New Item';
	        				self.items.push(self.item);
	        				self.item.id = response;
	        				self.selectedParentCategory={};
	        				self.item={id:null, name:'', sellingPrice:null, minimumNumberOfItems:null, numberOfitemsInContainer:null, storedLocation:''
	        					, countWord:'', category:null, modail:null, container:null, itemReference:null};
	        			},
	        			function(errResponse){
	        				console.error('Error while creating item from controller.');
	        			}	
	        	);
	        };

	        self.submitCategory = function() {
	        		if(self.category.id == null){
	        			self.createCategory(self.category);
	        			}
	        		else{
	        			self.updateCategory(self.category);
	        		}
	        };
	            
	        self.createCategory = function(category){
	        	ItemService.createCategory(category).then(
	        			function(response){
	        				self.reset();
	        			},
	        			function(errResponse){
	        				console.error('Error while creating Controller (from controller).');
	        			}	
	        	);
	        };

	        self.updateCategory = function(category){
	        	ItemService.updateCategory(category).then(
	        			function(response){
	        				self.reset();
	        			}, 
	        			function(errResponse){
	        				console.error('Error while updating Container.');
	        			});
	        };
	            
	        self.initNewCreateItem = function(){
	        	ItemService.getAllItemReference().then(
	        			function(response){
	        				self.allItemReference = response;
	        			},
    					function(errResponse){
    						console.error('Error while getting item references from controller');
    					});
	        	ItemService.getCurrentContainerCategories().then(
	        			function(response){
	        				self.currentContainerCategories = response;
	        				for (var i = 0; i < self.currentContainerCategories.length; i++) {
	        					self.currentContainerParentCategories.push(self.currentContainerCategories[i].parentCategory);
	        				}	
	        				self.currentContainerParentCategories = unique(self.currentContainerParentCategories);
	        			},
    					function(errResponse){
    						console.error('Error while getting current container categories from controller');
    					});
	        	ItemService.getCurrentContainerModails().then(
	        			function(response){
	        				self.currentContainerModails = response;
	        			},
    					function(errResponse){
    						console.error('Error while getting current container modails from controller');
    					});
	        };
	        
	        self.listItems = function(){
	        	ItemService.fetchAllItems().then(
	        			function(response){
	        				self.items = response;
	        			},
    					function(errResponse){
    						console.error('Error while fetching all items from controller');
    					});
	        };
	        
	        self.removeItem = function(id,index){
	        	ItemService.deleteItem(id).then(
	        			function(response){
	        				self.items.splice(index-1,1);
	        			},
    					function(errResponse){
    						console.error('Error while deleting item from controller');
    					});
	        };
	        
	        self.editItem = function(item){
	        	self.initNewCreateItem();
	        	self.item = item;
	        	self.filteredCategories = [];
	        	self.selectedParentCategory = self.item.category.parentCategory;
	        	if (self.selectedParentCategory != null) {
	        		for (var i = 0; i < self.currentContainerCategories.length; i++) {
						if(self.currentContainerCategories[i].parentCategory.id == self.selectedParentCategory.id){
							self.filteredCategories.push(self.currentContainerCategories[i]);
						}
					}
				}
	        	self.updateFilteredSelectedCategoryModails(self.item.category);
	        	self.createOrListItemsTitle = 'List Items';
	        	self.openCreateOrListItems = !self.openCreateOrListItems
	        };
	        
	        self.itemReferenceChanged = function(itemRef){
	        		self.item.itemReference = itemRef;
	        		self.item.name = self.item.itemReference.name;
	        };
	        
	        self.updateFilteredCategories = function(item){
	        	self.selectedParentCategory = item;
	        	self.filteredCategories = [];
	        	self.filteredSelectedCategoryModails=[];
	        	for (var i = 0; i < self.currentContainerCategories.length; i++) {
					if(self.currentContainerCategories[i].parentCategory.id == self.selectedParentCategory.id){
						self.filteredCategories.push(self.currentContainerCategories[i]);
					}
				}
	        };
	        
	        self.itemModailChanged = function(modail){
	        	self.item.modail = modail;
	        };
	        
	        self.updateFilteredSelectedCategoryModails = function(item){
	        	self.item.category = item;
	        	self.filteredSelectedCategoryModails=[];
	        	for (var i = 0; i < self.currentContainerModails.length; i++) {
	        		if (self.currentContainerModails[i].category.id == self.item.category.id) {
	        			self.filteredSelectedCategoryModails.push(self.currentContainerModails[i])
					}
				}
	        };
	        
	        self.updateCreateOrListItems = function(){
	        	self.openCreateOrListItems = !self.openCreateOrListItems;
	        	if (self.openCreateOrListItems) {
	        		self.createOrListItemsTitle = 'List Items';
	        		self.selectedParentCategory={};
	        		self.filteredCategories=[];
	        		self.filteredSelectedCategoryModails=[];
    				self.item={id:null, name:'', sellingPrice:null, minimumNumberOfItems:null, numberOfitemsInContainer:null, storedLocation:''
    					, countWord:'', category:null, modail:null, container:null, itemReference:null};
				}else{
					self.createOrListItemsTitle = 'Create New Item';
				}
		        
	        };
	        
	        self.pagingAct = function(text, page, pageSize, total) {
	        	console.info({
	                text: text,
	                page: page,
	                pageSize: pageSize,
	                total: total
	            });
	        };

	        
	        self.updateDisplayedItems = function(fromIndex, pageSize){
	        	for (var i = fromIndex; i < pageSize-1; i++) {
					displayedItems.push(angular.copy(items[i]));
				}
	        };
	        
	        self.reset = function(){
	        	self.category={id:null,name:'', parentCategory:null, containerType:null, isLeaf:true, binaryCode:''};
	        	$scope.myForm.$setPristine(); //reset Form
	        };
	        
	        var unique = function(origArr) {
	            var newArr = [],
	                origLen = origArr.length,
	                found, x, y;

	            for (x = 0; x < origLen; x++) {
	                found = undefined;
	                for (y = 0; y < newArr.length; y++) {
	                    if (origArr[x].id === newArr[y].id) {
	                        found = true;
	                        break;
	                    }
	                }
	                if (!found) {
	                    newArr.push(origArr[x]);
	                }
	            }
	            return newArr;
	        };
	        
			}]);


 