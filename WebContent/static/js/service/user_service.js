'use strict';

App.factory('UserService', ['$http', '$q', function($http, $q){
	var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/';
	
	var service = {
			fetchAllUsers:fetchAllUsers,
			createUser:createUser,
			updateUser:updateUser,
			saveUserContainerList:saveUserContainerList,
			deleteUser:deleteUser,
			fetchAllContainers:fetchAllContainers,
			getRoles:getRoles,
			getRolesAssignedToSpecificUser:getRolesAssignedToSpecificUser,
			ckUniqueValue:ckUniqueValue
	};
	
	return service;
		
			function fetchAllUsers() {
					return $http.get( REST_SERVICE_URI + 'allUsers/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching users');
										return $q.reject(errResponse);
									} );
			}
		    
		    function createUser(user, assignedroles){
		    	var params = {user:user ,assignedroles: assignedroles};
					return $http.post( REST_SERVICE_URI + 'createUser/', params).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating user');
										return $q.reject(errResponse);
									} );
		    }
		    
		    function updateUser(user, assignedroles){
		    	var params = {user:user ,assignedroles: assignedroles};
					return $http.put( REST_SERVICE_URI + 'updateUser/', params).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating user');
										return $q.reject(errResponse);
									} );
			}
		    
			function saveUserContainerList(userContainerList){
				return $http.put( REST_SERVICE_URI + 'assignUserRoles/', userContainerList).then(
						function(response){
							return response.data;
						}, 
						function(errResponse){
							console.error('Error while Assign user role from service');
							return $q.reject(errResponse);
						} );
			}
			
			function deleteUser(id){
					return $http.delete( REST_SERVICE_URI + 'deleteUser/'+id).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting user in service');
										return $q.reject(errResponse);
									} );
			}
			
			function fetchAllContainers() {
				return $http.get( REST_SERVICE_URI + 'allcontainers/').then(
								function(response){
									return response.data;
								}, 
								function(errResponse){
									console.error('Error while fetching containers');
									return $q.reject(errResponse);
								} );
			}
		
			function getRoles() {
				return $http.get( REST_SERVICE_URI + 'getRoles/').then(
							function(response){
									return response.data;
							}, 
							function(errResponse){
								console.error('Error while fetching roles');
								return $q.reject(errResponse);
							} );
			}
	
			function getRolesAssignedToSpecificUser(id) {
				return $http.get( REST_SERVICE_URI + 'getRolesAssignedToSpecificUser/'+id).then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while get roles assigned to specific user from service');
								return $q.reject(errResponse);
							} );
			}
	
			function ckUniqueValue(table, column, id, value) {
				var data = [table, column, id, value];
				return $http.post( REST_SERVICE_URI + 'checkUniqueValue/', data).then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								return errResponse.data;
								/*return $q.reject(errResponse);*/
							} );
			}
}]);
