'use strict';

App.factory('ContainerService', ['$http', '$q', function($http, $q){

	return {
		
			fetchAllContainers: function() {
					return $http.get('http://localhost:8081/BackEnd/allcontainers/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching containers');
										return $q.reject(errResponse);
									} );
			},
			containersType: function() {
				return $http.get('http://localhost:8081/BackEnd/containerstype/').then(
								function(response){
									return response.data;
								}, 
								function(errResponse){
									console.error('Error while getting containers type from service');
									return $q.reject(errResponse);
								} );
			},
		    
		    createContainer: function(container){
					return $http.post('http://localhost:8081/BackEnd/newcontainer/', container).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating container');
										return $q.reject(errResponse);
									} );
		    },
		    
		    updateContainer: function(container){
					return $http.put('http://localhost:8081/BackEnd/updateContainer/', container).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating container from service');
										return $q.reject(errResponse);
									} );
			},
		    
			deleteContainer: function(id){
					return $http.delete('http://localhost:8081/BackEnd/deleteContainer/'+id).then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting container');
										return $q.reject(errResponse);
									} );
			}
	};

}]);
