'use strict';

App.factory('ItemService', ['$http', '$q', function($http, $q){

			return {
				
				createOrUpdateItem: function(item){
					return $http.post('http://localhost:8081/BackEnd/createOrUpdateItem/',item).then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while create or update item From Service');
								return $q.reject(errResponse);
							} );
				},
				
				getCurrentContainerCategories: function() {
					return $http.get('http://localhost:8081/BackEnd/getAllCategoriesForLoginnedContainer/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching Parent Categories From Service');
										return $q.reject(errResponse);
									} );
				},
				
				deleteItem: function(id){
					return $http.delete('http://localhost:8081/BackEnd/deleteItem/'+id).then(
							function(response){
								return response.data;
							}, 
							function(errResponse){
								console.error('Error while delete item From Service');
								return $q.reject(errResponse);
							} );
				},
				
				getCurrentContainerModails: function() {
					return $http.get('http://localhost:8081/BackEnd/getAllModailsForLoginnedContainer/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching Parent Categories From Service');
										return $q.reject(errResponse);
									} );
				},
				
				getAllItemReference: function() {
					return $http.get('http://localhost:8081/BackEnd/getAllItemReference/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching Item Reference From Service');
										return $q.reject(errResponse);
									} );
				},
				
				fetchAllItems: function() {
					return $http.get('http://localhost:8081/BackEnd/fetchAllItems/').then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching all Items From Service');
										return $q.reject(errResponse);
									} );
				}
			};
		}]);
