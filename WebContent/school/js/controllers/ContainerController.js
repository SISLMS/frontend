angular.module('MetronicApp').controller('ContainerController', function($rootScope, $scope, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    
    var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/';

	
    var self = this;
    self.container={id:null,name:'',containerType:'',address:'',suspended:''};
    self.containers=[];
    self.containersType=[];
    self.duplicate_container_name = false;
    self.update_duplicate_container_name = false;
    self.openEditContainerForm = false;
    
    
    self.fetchAllContainers = function(){
    	$http.get( REST_SERVICE_URI +'allcontainers/').then(
					       function(d) {
						        self.containers = d.data;
					       },
      					function(errResponse){
      						console.error('error while fetching all containers from controller');
      					}
			       );
    };
    
    self.getContainersType = function(){
    	$http.get( REST_SERVICE_URI +'containerstype/').then(
					       function(d) {
						        self.containersType = d.data;
					       },
      					function(errResponse){
      						console.error('Error while getting containers type from controller');
      					}
			       );
    };
    
    
    self.submit = function() {
    	//self.createContainer(self.container);
    	if(self.container.id == null){
		  self.createContainer(self.container);
    	}
    	else{
		  self.updateContainer(self.container);
    	}
      };
    	
    self.createContainer = function(container){
    	$http.post( REST_SERVICE_URI +'newcontainer/', container).then(
			              function(response){
			            	  self.duplicate_container_name = false;
			            	  self.reset();
							},
			              function(errResponse){
								if(errResponse.headers().exception == 'container_name_duplicated'){
									alert("The container name "+container.name+" is aleardy exists in db");
									self.duplicate_container_name = true;
								}
				               console.error('Error while creating Controller (from controller).');
			              }	
            );
    };

    self.updateContainer = function(container){
    	$http.put( REST_SERVICE_URI +'updateContainer/', container).then(
	            		  function(response){
	            			  self.fetchAllContainers();
		            		  self.openEditContainerForm = ! self.openEditContainerForm;
		            		  }, 
			              function(errResponse){
				               console.error('Error while updating Container.');
			              });
    };

    self.deleteContainer = function(id){
    	$http.delete( REST_SERVICE_URI +'deleteContainer/'+id).then(
			              self.fetchAllContainers, 
			              function(errResponse){
				               console.error('Error while deleting Container.');
			              }	
            );
    };

    self.edit = function(id){
  	  for(var i = 0; i < self.containers.length; i++){
            if(self.containers[i].id == id) {
               self.container = angular.copy(self.containers[i]);
               break;
            }
        };
        self.openEditContainerForm = ! self.openEditContainerForm;
//  	  var box = $("#mb-update-container");
//        box.addClass("open");
//        box.find(".mb-control-yes").on("click",function(){
//      	  ContainerService.updateContainer(self.container, self.container.id)
//            .then(
//          		  function(response){
//		            	  self.fetchAllContainers(); 
//		            	  self.update_duplicate_container_name = false;
//		            	  box.removeClass("open");
//			              console.error('Error while updating Container.');
//		              }	,
//		              function(errResponse){
//							if(errResponse.headers().exception == 'update_container_name_duplicated'){
//								alert("The container name "+self.container.name+" is aleardy exists in db");
//								self.update_duplicate_container_name = true;
//							}
//			               console.error('Error while creating Controller (from controller).');
//		              }	
//        );
//        console.log('id to be edited', id);
//        });
//        box.find(".mb-control-close").on("click",function(){
//      	  box.removeClass("open");
//        });
            
    };
    
    self.remove = function(id){
  	  var box = $("#mb-remove-row");
        box.addClass("open");
        box.find(".mb-control-yes").on("click",function(){
            box.removeClass("open");
            self.deleteContainer(id); 
        });
        box.find(".mb-control-close").on("click",function(){
            box.removeClass("open");
        });
    };

    self.reset = function(){
  	  self.container={id:null,name:'',containerType:'',address:'',suspended:''};
        $scope.myForm.$setPristine(); //reset Form
        self.openEditContainerForm = ! self.openEditContainerForm;
    };
    
});