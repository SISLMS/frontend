angular.module('MetronicApp').controller('UserController', function($stateParams,$rootScope, $scope, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    
    var REST_SERVICE_URI = 'http://localhost:8081/BackEnd/';
	
    var self = this;
    self.user = {id:null, userName:'', address:'', name:'', email:'', gender:'0', suspended:'',birthDate:'', password:'',containerRoleIds:[]};
    self.container = {id:null , name:'' , suspended:'' , address:''};
    self.role = {id:null , name:'' , description:''};
    self.users = [];
    self.containers=[{id:'',name:'',containerType:'',role:[],showassign:false}];
    self.Roles=[];
    self.duplicate_user_name = false;
    self.update_duplicate_user_name = false;
    self.UserContainerList = [{user:'' , container:'' , userContainerRole:[]}];
    self.openEditUserForm = false;
    self.containersWithThierRoles = [];
    
    // This array will contain containerId and the roleId (assigned to user)
    self.assignedroles=[];
    
    // This array will contain list of roles in container assigned to user before submit
    self.listofassignedroles=[];
    
    self.fetchAllUsers = function(){
    	$http.get( REST_SERVICE_URI + 'allUsers/').then(function(d) {
						        self.users = d.data;
            					},
      			function(errResponse){
            						alert(REST_SERVICE_URI + 'allUsers/');
      						console.error('Error while fetching Currencies');
            					});
        };
        
    self.submit = function() {
    	
    	var selectedNodes = [];
  		self.user.containerRoleIds = [];
  		selectedNodes = $(allContainersAndThierRoles).jstree().get_selected(true);
  		for (var i = 0; i < selectedNodes.length; i++) {
  			if(selectedNodes[i].id.indexOf('_') < 0){
  				self.user.containerRoleIds.push(selectedNodes[i].id);
  			}
		} 
  		
  	  if(self.user.id == null){
  		self.createUser(self.user);
  	  }
  	  else{
  		  self.updateUser(self.user);
  	  }
    };
    
    self.createUser = function(user){
  	self.user.password=$scope.pw1;
  		$http.post( REST_SERVICE_URI + 'createUser/', user).then(
      		  		  function(response){
		            		  $('#allContainersAndThierRoles').jstree("deselect_all");
		            		  self.reset();
		                      $scope.pw1="";
		                      $scope.pw2="";
		                      if(user.id != null){
		                      for(var i = 0; i < self.users.length; i++){
		                          if(self.users[i].id == user.id) {
		                        	  self.users[i] = angular.copy(user);
		                             break;
		                          }
		                      };
		                      }else{
		                    	  self.users.push(user);
		                      } },
			              function(errResponse){
		                    	  console.error('Error while creating User (from controller).');
			              } );
    };

   self.updateUser = function(user){
	   		$http.put( REST_SERVICE_URI + 'updateUser/', user).then(
      		  		  function(response){
	            		  self.fetchAllUsers();
	            		  }, 
			              function(errResponse){
				               console.error('Error while updating User.');
			              });
    };

   self.deleteUser = function(id){
	   $http.delete( REST_SERVICE_URI + 'deleteUser/'+id).then( 
      		  		  self.fetchAllUsers, 
			              function(errResponse){
				               console.error('Error while deleting User in controller.');
			              });
    };

    self.edit = function(id){
  	  for(var i = 0; i < self.users.length; i++){
            if(self.users[i].id == id) {
               self.user = angular.copy(self.users[i]);
               break;
            }
        };
        $scope.pw1 = self.user.password;
        $scope.pw2 = self.user.password;
        self.getRolesAssignedToSpecificUser(id);
    };
    
    self.savePriviledges = function(){
  	  self.assignedroles = [];
  	  for(var i = 0; i < self.containers.length; i++){
  		  for(var x = 0; x < self.containers[i].role.length; x++){
  			self.assignedroles.push({userId:self.user.id,containerId:self.containers[i].id,roleId:self.containers[i].role[x].id});
            }
  	  }
  	  if (self.assignedroles.length === undefined ||self.assignedroles.length == 0) {
  		  self.assignedroles.push({userId:self.user.id,containerId:'',roleId:''});
  	  }
  	  $http.put( REST_SERVICE_URI + 'assignUserRoles/', userContainerList).then(
      		  function(response){
      			  self.getRolesAssignedToSpecificUser(self.user.id);
					},
	              function(errResponse){
		               console.error('Error while Assigning User Roles (from controller).');
	              });
    };
    
    
    self.getRolesAssignedToSpecificUser = function(id){
    	$http.get( REST_SERVICE_URI + 'getRolesAssignedToSpecificUser/'+id).then(
			       function(response) {
			    	   self.UserContainerList = response.data;
			    	   self.listofassignedroles = [];
			        	  self.assignedroles = [];
			        	  self.fetchAllRoles();
			        	  for (var x = 0; x < response.length; x++) {
			        			  var listofroles = [];
			        			  	for(var i = 0; i < response[x].userContainerRole.length; i++){
			        			  					var role = { id:response[x].userContainerRole[i].role.id ,name:response[x].userContainerRole[i].role.name , desc:response[x].userContainerRole[i].role.description };
			        			  					listofroles.push(role);
			        			  	}
			        	  var containerrole = {containername:response[x].container.name ,role:listofroles};
			        	  self.listofassignedroles.push(containerrole);
			          
			        	  }
			    	   self.openEditUserForm = ! self.openEditUserForm;
			       },
				function(errResponse){
					console.error('Error while get roles assigned to specific user from controller ');
				}
	       );
    };
    
    self.remove = function(id){
  	  var box = $("#mb-remove-row");
        box.addClass("open");
        box.find(".mb-control-yes").one("click",function(){
            box.removeClass("open");
            self.deleteUser(id); 
        });
        box.find(".mb-control-close").on("click",function(){
            box.removeClass("open");
        });
    };

    
    self.reset = function(){
  	  self.user={id:null,userName:'',address:'',name:'',email:'',suspended:'',password:''};
  	  self.repassword='';
        $scope.myForm.$setPristine(); //reset Form
    };
    
    self.fetchAllContainers = function(){
    	$http.get( REST_SERVICE_URI + 'allcontainers/').then(
  			  function(result) {
					        self.containers = result.data;
					        	for(var i = 0; i < self.containers.length; i++){
    						    	self.containers[i].role=[];
							}
					      
					    for(var i = 0; i < self.containers.length; i++){
					    	for(var x = 0; x < self.listofassignedroles.length; x++){
					    		if(self.containers[i].name === self.listofassignedroles[x].containername){
					    			for(var y = 0; y < self.listofassignedroles[x].role.length; y++){
					    				self.containers[i].role.push({id:self.listofassignedroles[x].role[y].id});
					    			}
					    		}
					    	}
			             }
				       },
  					function(errResponse){
  						console.error('Error yyyyy');
  					}
		       );
    };
    
    self.listContainersToUser = function(){
  	  self.fetchAllContainers();
  	  self.fetchAllRoles();
  	  var box = $("#assign-user-to-containers");
        box.addClass("open");
        box.find(".mb-control-yes").on("click",function(){
        box.removeClass("open");
        });
        box.find(".mb-control-close").on("click",function(){
      	  box.removeClass("open");
        });
    };
    self.assignRolesToUser = function(){
  	  self.listofassignedroles = [];
  	  self.assignedroles = [];
  	  for (var x = 0; x < self.containers.length; x++) {
  		  if (self.containers[x].role.length > 0) {
  			  var listofroles = [];
  			  	for(var i = 0; i < self.containers[x].role.length; i++){
  			  		var addrole ={ containerId:self.containers[x].id , roleId:self.containers[x].role[i].id };
  			  		self.assignedroles.push(addrole);
  			  			for(var s = 0; s < self.Roles.length ; s++){
  			  				if(self.containers[x].role[i].id == self.Roles[s].id){
  			  					var role = { id:self.Roles[s].id ,name:self.Roles[s].name , desc:self.Roles[s].description };
  			  					listofroles.push(role);
  			  					}
  			  			}
  			  	}
  	  var containerrole = {containername:self.containers[x].name ,role:listofroles};
  	  self.listofassignedroles.push(containerrole);
    
  		  }
  	  }
	  };
    
    
    
    self.fetchAllRoles = function(){
    	$http.get( REST_SERVICE_URI + 'getRoles/').then(
				       function(result) {
				    	 self.Roles = result.data;
				       },
  					function(errResponse){
  						console.error('Error in get containers roles');
  					}
		       );
         
    };
    
    self.dropdownMltiselectSettings = {
		    smartButtonMaxItems: 3,
		    enableSearch: true,
		    smartButtonTextConverter: function(itemText, originalItem) {
		        if (itemText === 'Jhon') {
		        return 'Jhonny!';
		        }
		        return itemText;
		    }
		};
    
    self.assigncontainertouser = function(container){
  	  if(container.role.length > 0){
  		  for(var i = 0; i < self.assignedroles.length; i++){
      		   if(container.id === self.assignedroles[i].containerId){
      			   self.assignedroles.splice(i,1);
      			   i--;
      			   container.showassign=false;
      		   }
      	  }
  		  for(var i =0 ; i < self.listofassignedroles.length ; i++){
      		  if(container.name == self.listofassignedroles[i].containername){
      			  self.listofassignedroles.splice(i,1);
     			   	  i--;
      		  }
      	  }
  	  var listofroles = [];
  	  for(var i = 0; i < container.role.length; i++){
  		  var addrole ={ containerId:container.id , roleId:container.role[i].id };
  		  self.assignedroles.push(addrole);
  		  
  		  for(var s = 0; s < self.Roles.length ; s++){
  			  if(container.role[i].id == self.Roles[s].id){
  				  var role = { name:self.Roles[s].name , desc:self.Roles[s].description };
  				  listofroles.push(role);
  			  }
  		  }
            }
  	  
  	  var containerrole = {containername:container.name ,role:listofroles};
  	  self.listofassignedroles.push(containerrole);
  	  
  	  if(container.role.length > 0){
  		  container.showassign=true;
  	  }
    }
  	  else{
  		  alert("Please Select Role");
  	  }
    };
    
    self.disableassigncontainer = function(container){
  	  container.role=[];
  	  for(var i = 0; i < self.assignedroles.length; i++){
  		   if(container.id === self.assignedroles[i].containerId){
  			   self.assignedroles.splice(i,1);
  			   i--;
  			   container.showassign=false;
  		   }
  	  }
  	  
  	  for(var i =0 ; i < self.listofassignedroles.length ; i++){
  		  if(container.name == self.listofassignedroles[i].containername){
  			  self.listofassignedroles.splice(i,1);
 			   	  i--;
  		  }
  	  }
  	  
    };
    
    self.emptylistofassignedroles = function(){
  	  self.listofassignedroles = [];
  	  self.assignedroles =[];
  	  alert("Roles In Containers To This User Is Removed .... ");
    };
    
    self.currentroles = function(container){
  	  if(self.assignedroles.length == 0){
  		  alert("No Roles.....!");
  	  }
    };
    
    self.showRolesAndDescription = function(){
  	  $(".faq .faq-item .faq-title").click(function(){        
  	        var item = $(this).parent('.faq-item');
  	        
  	        if(item.hasClass("active"))
  	            $(this).find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
  	        else
  	            $(this).find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
  	        
  	        item.toggleClass("active");
  	        
  	        onresize(300);
  	    });
    };
    
    self.initiateUserForm = function(){
    	var loggedInUserId = 1;
    	if ($stateParams.updateUserID != undefined) {
    		
    		console.error($stateParams.updateUserID);
    		var userId = $stateParams.updateUserID;
    		$http.get( REST_SERVICE_URI + 'getUserById/'+userId).then(
    				function(response) {
			              self.user = response.data;
			              $scope.pw1 = self.user.password;
			              $scope.pw2 = self.user.password;
			              if (self.user.gender == 'MALE') {
			            	  self.user.gender = '0';
						}else {
							self.user.gender = '1';
						}
			              
			              $http.get( REST_SERVICE_URI + 'getContainersWithThierRoles/'+loggedInUserId).then(
			   			       function(xas) {
			   			    	   self.containersWithThierRoles = xas.data;
			   			    	   if (self.user.containerRoleIds.length >0) {
			   			    		   for (var a = 0; a < self.containersWithThierRoles.length; a++) {
			   			    			   for (var z = 0; z < self.containersWithThierRoles[a].children.length; z++) {
			   			    				for (var b = 0; b < self.user.containerRoleIds.length; b++) {
			   			    					console.log(self.containersWithThierRoles[a].children[z].id +' == '+self.user.containerRoleIds[b]);
				   								if (self.containersWithThierRoles[a].children[z].id == self.user.containerRoleIds[b]) {
				   									self.containersWithThierRoles[a].children[z].state = {"selected": true};
				   								}
				   							}
										}
			   						}
			   			    	   }
			   			    	   $('#allContainersAndThierRoles').jstree({
			   			               'plugins': ["wholerow", "checkbox", "types"],
			   			               'core': {
			   			                   "themes" : {
			   			                       "responsive": false
			   			                   },    
			   			                   'data': self.containersWithThierRoles
			   			               },
			   			               "types" : {
			   			                   "default" : {
			   			                       "icon" : "fa fa-folder icon-state-warning icon-lg"
			   			                   },
			   			                   "file" : {
			   			                       "icon" : "fa fa-file icon-state-warning icon-lg"
			   			                   }
			   			               }
			   			           });
			   			       },
			   				function(errResponse){
			   					console.error('Error in get containers With Thier roles');
			   				}
			   	       );
			              
    				},
					function(errResponse){
						console.error('Error in assign user to update');
    				}
    				);
    		
		};
		if ($stateParams.updateUserID == undefined) {
    	$http.get( REST_SERVICE_URI + 'getContainersWithThierRoles/'+loggedInUserId).then(
			       function(xas) {
			    	   self.containersWithThierRoles = xas.data;
			    	   if (self.user.containerRoleIds.length >0) {
			    		   for (var a = 0; a < self.containersWithThierRoles.length; a++) {
							for (var b = 0; b < self.user.containerRoleIds.length; b++) {
								if (self.containersWithThierRoles[a].id == self.user.containerRoleIds[b].id) {
									self.containersWithThierRoles[a].state.selected = true;
								}
							}
						}
			    	   }
			    	   $('#allContainersAndThierRoles').jstree({
			               'plugins': ["wholerow", "checkbox", "types"],
			               'core': {
			                   "themes" : {
			                       "responsive": false
			                   },    
			                   'data': self.containersWithThierRoles
			               },
			               "types" : {
			                   "default" : {
			                       "icon" : "fa fa-folder icon-state-warning icon-lg"
			                   },
			                   "file" : {
			                       "icon" : "fa fa-file icon-state-warning icon-lg"
			                   }
			               }
			           });
			       },
				function(errResponse){
					console.error('Error in get containers With Thier roles');
				}
	       );
		};
      };
    
});