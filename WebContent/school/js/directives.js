/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
MetronicApp.directive('ngSpinnerBar', ['$rootScope', '$state',
    function($rootScope, $state) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function(event) {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setAngularJsSidebarMenuActiveLink('match', null, event.currentScope.$state); // activate selected link in the sidebar menu
                   
                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);     
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
])

// Handle global LINK click
MetronicApp.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});

// Handle Dropdown Hover Plugin Integration
MetronicApp.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };  
});

// Confirm PassWord
MetronicApp.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    // console.info(elem.val() === $(firstPassword).val());
                    ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                });
            });
        }
    }
}]);

// Check Unique Value
MetronicApp.directive("uniqueValue", ['$http',function($http) {
	  return {
	    restrict: 'A',
	    require: 'ngModel',
	    link: function (scope, element, attrs, ngModel) {
	      element.bind('blur', function (e) {
	        if (!ngModel || !element.val()) return;
	        var keyProperty = scope.$eval(attrs.uniqueValue);
	        var currentValue = element.val();
	        var data = [keyProperty.table, keyProperty.column, keyProperty.id, currentValue];
	        $http.post( 'http://localhost:8081/BackEnd/checkUniqueValue/', data)
	          .then(function (unique) {
	            //Ensure value that being checked hasn't changed
	            //since the Ajax call was made
	            if (currentValue == element.val()) {
	            	if (unique.data == true) {
	            		console.log('unique = '+unique.data);
	  	                ngModel.$setValidity('unique',unique.data);
					}else{
						console.log('unique = '+unique.messageText);
	  	                ngModel.$setValidity('unique',false);
					}
	              scope.$broadcast('show-errors-check-validity');
	            }	            
	          },
              function(errResponse){
	        	  for (var i = 0; i < errResponse.data.length; i++) {
	        		  console.log('Error = '+errResponse.data[i].exceptionKey);
				}
//	        	  console.log('unique = '+unique.messageText);
	              ngModel.$setValidity('unique',false);
	              scope.$broadcast('show-errors-check-validity');
	          }	);
	      });
	    }
	  }
	}]);
